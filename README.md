# Safety System

A monitoring system for wearing helmet detection invented using the YOLO v5 algorithm and its implementation in the Raspberry Pi, along with applications and alarm functions.

## Train and use machine learning model

1. First, if you want to train your own dataset, you need to make sure you have the right environment. Most importantly, you need to install Python, Pytorch, PyQt5, PyQtChart, PyQt5-tools, GPUtil.You can use a recommended dataset on GitHub.

- [ ] [Dataset](https://github.com/njvisionpower/Safety-Helmet-Wearing-Dataset)

2. We want to determine the three types of categories and create our own dataset configuration file custom_data.yaml. The code looks like the following.

```
train: ./score/images/train
val: ./score/images/val

# number of classes
nc: 3

# class names
names: ['person', 'head', 'helmet']
```

3. The file tree needs to be placed as shown in the following image.

![file tree](/images/53bac46d05e8d0cf7469995f1f27f3a.png)

4. Select a model you need in the folder . /models and make a copy of it. Change the nc= at the beginning of the file to the number of categories in the dataset. 

5. And since everything is OK, we can start to train the model. Enter the following code in the terminal for training, we have chosen to use the yolov5s model with weights of yolov5s.pt.

```
python train.py --img 640 \
                --batch 16 --epochs 10 --data ./data/custom_data.yaml \
                --cfg ./models/custom_yolov5.yaml --weights ./weights/yolov5s.pt
```

6. After training, the weights are saved as best.pt in ./runs/exp/weights. You can also see the results of the training in the exp folder.

If you don't want to train your own dataset. We have already trained one. You can find it in...

7. You can try to apply the model detection in your PC. You can enter the following code in your terminal to detect images, videos and video streams, etc.

```
python detect.py --source   0  # webcam
                            file.jpg  # image 
                            file.mp4  # video
                            path/  # directory
                            path/*.jpg  # glob
                            rtsp://170.93.143.139/rtplive/470011e600ef003a004ee33696235daa  # rtsp stream
                            http://112.50.243.8/PLTV/88888888/224/3221225900/1.m3u8  # http stream
```

For example, to detect images using the best weights, you can run the following command in the terminal.

```
python detect.py --source path_of_the_image --weights ./weights/best.pt

```

After running the code, the detected images will be saved in the ./inference/output/ folder.

## How to run the model on a Raspberry Pi and successfully implement detection and raise an alarm

1. You need all the necessary materials. In our prototype, we used a Raspberry Pi 4B, Dupont cables, a breadboard, a resistor, an LED, a buzzer, a keyboard, a mouse, and an Android phone.

2. Again, to successfully run this machine learning model on the Raspberry Pi, you will need to install all the required libraries. One of the most important is Pytorch. Unlike in Windows, it is not an easy task to install pytorch in Raspberry Pi. You can refer to the following URL to install, during the installation process you may need to downgrade the version of  python in the Raspberry Pi to 3.7.

- [ ] [Install pytorch on Raspberry Pi](https://gist.github.com/akaanirban/621e63237e63bb169126b537d7a1d979)

3. Next you can implement the following code in detect.py to automatically send an email to the administrator when a construction worker is not wearing a helmet. 

```
my_sender = '1165047323@qq.com'  # Sender Email Address

my_pass = 'dseqjiforwhkiaha'  # Sender Email password

my_user = '1165047323@qq.com'  # Recipient Email Address


def mail():
    ret = True

    try:
        msg = MIMEText('!!!Attention!!!, someone is NOT wearing a helmet', 'plain', 'utf-8') # Email content

        msg['From'] = formataddr(["Helmet Detection", my_sender])  # Sender nickname

        msg['To'] = formataddr(["FK", my_user])  

        msg['Subject'] = "Helmet Warning"   # Email title
        
        server = smtplib.SMTP("smtp.qq.com", 25)  

        server.login(my_sender, my_pass) 

        server.sendmail(my_sender, [my_user, ], msg.as_string())  
        server.quit()  

    except Exception:  
        ret = False

    return ret

```

In the above code, one can customize the sender's email account, sender's email nickname, the recipient's email account, email title and email content. In our prototype, we used qq email, which is more commonly used in China.

4. Add code to detect.py to call the mail function when the label of the object is "Head". And add code to set the GPIO pins of Raspberry Pi. One can use the code below. We have to dectect.py files, which dectect.py is for buzzer and detect2.py is for LED.

```
if names[int(cls)] == "head":
                    mail()
                    
                    GPIO.setmode(GPIO.BOARD)
                    GPIO.setup(16, GPIO.OUT)
                    i = 1
                    for i in range(0, 50):
                        GPIO.output(16, GPIO.LOW)
                        time.sleep(0.1)
                        GPIO.output(16, GPIO.HIGH)
                        time.sleep(0.1)
                        
                        i += 1
                   
                    GPIO.cleanup()
```

## Connect the Raspberry Pi and LED or buzzer

1. Firstly, to realize the connection between Raspberry Pi and LED. Unlike incandescent lamps without polarity, LEDs can only emit light when a forward current flows. When direct voltage is connected, a larger current flows, which is called forward bias. So it is important to connect the pins with the Raspberry Pi correctly, considering the polarity. With the help of relative material, we decided to choose the encoding mode of “Board” as the sequence below:

![GPIO pins](/images/2df14dc4223f23a910b2557ca7ae1a8.png)

Then we choose Pin 16 as the signal output, through which the High/Low signal levels are generated to control the LED and can be regarded as the current positive direction. To implement the circuit in a more logical way, the breadboard is applied to connect the elements. 
Specially, since the output voltage of the GPIO in Raspberry Pi is 3.3V, while the working voltage range of red light emitting diode is from 1.5V to 2.6V and the maximum working current does not exceed 22mA, so the resistor of 220Ω can be used to divide the total voltage from output.
Totally 3 Dupont cables are used to connect the elements (LED, resistor, Raspberry Pi) together, and the output of GPIO Pin 16 is previously defined as positive side, so the longer leg of the LED light (the anode of the diode) should be connected with this side with the red/orange Dupont cable. On the contrary, the shorter leg of the LED is the negative side and should be connected to the GND(Ground) Pin 14 with the black Dupont cable.

2. Secondly, we implement the setup with an active buzzer, see the Figure below. Unlike the LED, by connecting the buzzer three cables for three interfaces as well as three pins on the Raspberry Pi are used.

![buzzer](/images/22f60fc45efdf2408cedb4137ce94ac.png)

## Using IP Webcam APP as camera

1. Download a web camera app. We recommend to download an app called IP Webcam on Google play. 

![IP Webcam](/images/14311214523bab6a6c6b92ece92fd34.jpg)

Open the APP, click Start server on the bottom of the home page, and the camera will be open, one can see the IP address on the bottom.

![IP Webcam 2](/images/d3e2efc2ef43f8c484507cfd8993c1b.jpg)

Now, all the preparations are done. We can open the terminal of the Raspberry Pi and call detect.py to start the detection. You can use the following code as an example.

```
python detect.py --source http://192.168.0.107:8080/video --weights best.pt

```

## Download our safetysystem app

You can download our APP from the .apk file above. 

### Android Studio Environment
 classpath 'com.android.tools.build:gradle:7.3.1'
 
